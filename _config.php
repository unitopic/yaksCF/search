<?php
$shortcuts_prefix = "!";
$shortcuts = [
  // YOUTUBE
  "yt" => [
    // URL / (url)[query]
    "https://www.youtube.com/results?search_query=",
    // Name
    "YouTube"
  ],
  // WIKIPEDIA
  "w" => [
    "https://en.wikipedia.org/w/index.php?search=",
    "Wikipedia (EN)"
  ],
  "wen" => [
    "https://en.wikipedia.org/w/index.php?search=",
    "Wikipedia (EN)"
  ],
  "wes" => [
    "https://es.wikipedia.org/w/index.php?search=",
    "Wikipedia (ES)"
  ],
  // GITHUB
  "gh" => [
    "https://github.com/search?q=",
    "GitHub"
  ],
  // DUCKDUCKGO
  "d" => [
    "https://duckduckgo.com/?q=",
    "DuckDuckGo"
  ],
  // GOOGLE
  "g" => [
    "https://www.google.com/search?q=",
    "Google"
  ],
  // TWITTER
  "tw" => [
    "https://twitter.com/search?q=",
    "Twitter"
  ],
  // STACK OVERFLOW
  "so" => [
    "https://stackoverflow.com/search?q=",
    "Stack Overflow"
  ]
];
$servers = [
  "https://repl.db.maotei.ga"
];