<?php include $_ENV["ROOT"] . "_config.php";
if (isset($_GET["q"]) && $_GET["q"]) {
  $q = json_decode(file_get_contents("https://repl.db.maotei.ga/search?query=" . urlencode($_GET["q"])));
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>"<?= htmlentities($_GET["q"]) ?>" | Maotei</title>
<link rel="icon" href="img/brown2.png" />
<link rel="stylesheet" href="styles.css" />
</head>
<body id="query">
<div id="navbar"><a href="submit.php">Submit</a></div>
<div id="center">
<h2><a href="/"><img src="img/brown2.png" width="20px" height="auto" alt="Kitten not found" /> Maotei</a></h2>
<form method="GET"><input type="text" name="q" id="q" placeholder="Stuff" value="<?= htmlentities($_GET["q"]) ?>"><input type="submit" value="SRCH!"></form>
<div id="list">
<?php
$c = False;
foreach ($q as $result) {
  echo '<div class="result"><a href="http://' . $result->{"url"} . '"><span class="title">' . htmlentities($result->{"title"}) . '</span><br /><span class="url">' . $result->{"url"} . '</span></a><br /><span class="desc">' . htmlentities($result->{"description"}) . "</span></div>";  
  echo '</div></a>';
  $c = True;
}
if (!$c) {
  echo "<h3>No results...</h3>";
} ?>
</div>
</div>
</body>
</html>
<?php
} else {
  header("location: /");
}