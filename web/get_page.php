<?php
set_error_handler(function(){});

function extract_title($data){
$start = "<title";
$end = "</title>";
$b = strpos($data,$start);
$a = strpos($data,$end,$b);
$length = $a - $b+10;
$title_temp = substr($data,$b,$length);


$start = ">";
$end = "</title>";
$b = strpos($title_temp,$start);
$a = strpos($title_temp,$end,$b);
$length = $a - $b+7;
$title = substr($title_temp,$b+1,$length-8);
//echo htmlentities($title);

return $title;
}

function extract_description($data){
$description = "";
foreach(preg_split("/[<>]+/",$data) as $line){
$description_txt = "/" . "description" . "/i";
$content = "/" . "content=" . "/i";
if(preg_match($description_txt,$line)&&preg_match($content,$line)){
$start = "content=";
$end = '"';
$b = strpos($line,$start);
$a = strpos($line,$end,$b+9);
$length = $a - $b-9;
$sub_line = substr($line,$b+9,$length);

$sub_line_txt = "/" . $sub_line . "/i";
if(!(preg_match($sub_line_txt,$description))){ 
$description .= $sub_line . " ";
}
}
}
return $description;
}

function extract_paragraph($data){
$start = "<p>";
$end = "</p>";
$b = strpos($data,$start);
$a = strpos($data,$end,$b+3);
$length = $a - $b-6;
$title = substr($data,$b,$length);
//echo htmlentities($title);

$paragraph = "";
foreach(preg_split("/[<]+/",$title) as $line){
$start2 = ">";
$b2 = strpos($line,$start2);
$sub_line = trim(substr($line,$b2+1));
$paragraph .= $sub_line . " ";
}
return $paragraph;
}

function extract_links($data){

		$addresses = "";
		$link_string =  "/" . "<a" . "/i";		
		while(preg_match($link_string,$data)){
		$start = "<a";
		$end = ">";
		$beginningPos = strpos($data, $start);
		$b = $beginningPos;
		$endPos = strpos($data, $end,$b);
		$a = $endPos;
		$lengte = $a-$b+1;
		$linkje = trim(substr($data, $b+1,$lengte));
		
		//ONLY EXTERNAL LINKS:
		
		$http_string =  "/" . "http:" . "/i";
		$https_string =  "/" . "https:" . "/i";				
		if(preg_match($http_string,$linkje)||preg_match($https_string,$linkje)){
		
		$start = "href=";
		$end = '"';
		$beginningPos = strpos($linkje, $start);
		$b = $beginningPos;
		$endPos = strpos($linkje, $end,$b+6);
		$a = $endPos;
		$lengte = $a-$b-6;
		$linkje_small = trim(substr($linkje, $b+6,$lengte));
		
		$start = "http";
		$end = "?";
		$beginningPos = strpos($linkje_small, $start);
		$b = $beginningPos;
		$endPos = strpos($linkje_small, $end,$b+6);
		$a = $endPos;
		$lengte = $a-$b;
		$linkje_small2 = trim(substr($linkje_small, 0,$lengte));
		if($linkje_small2 == ""){
		$real_linkje = $linkje_small;
		}
		else{
		$real_linkje = $linkje_small2;
		}
		
		$link_start = substr($real_linkje, 0,4);
		if($link_start=="http"){
		$addresses .= ($real_linkje . "\n");		
		}
		}
		
		$remove = $linkje;
		$data = str_replace($remove, "",$data);
		}

				
   		return  $addresses;

}



if($_SERVER["REQUEST_METHOD"] == "GET"){

$address = $_GET["name"];

$headers = @get_headers($address);
$code = "";
foreach ($headers as $x) {
  if ($x == (
    "HTTP/0.9 200 OK" ||
    "HTTP/1.0 200 OK" ||
    "HTTP/1.1 200 OK"
  )) {
    $code = $x;
    break;
  }
}
if(!$headers || !$code) {
	echo "URL doesn't exist.";
} else {

$html = file_get_contents($address);


$title = extract_title($html);
//echo htmlentities($title);
$description = extract_description($html);
if($description ==""){
$description = extract_paragraph($html);
}

$links = extract_links($html);


$logs = [
  urlencode($address),
  urlencode($title),
  urlencode($description),
  urlencode($links)
];



$title = substr($title,0,200);
//echo htmlentities($title);
$description = substr($description,0,400);



$data = [
  "KEY" => $_ENV["KEY"],
  "url" => $address,
  "title" => $title,
  "description" => $description
];

$options = [
    'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    ]
];
$context  = stream_context_create($options);
$result = file_get_contents("https://repl.db.maotei.ga/index_web", false, $context);


$list_of_links = file_get_contents('list_of_links.txt');
$list_of_links .= $links; 
file_put_contents('list_of_links.txt',$list_of_links);
header("location: submit.php?address=$logs[0]&title=$logs[1]&description=$logs[2]&links=$logs[3]");
}
}
?>