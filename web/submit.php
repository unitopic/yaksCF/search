<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Maotei - Submit</title>
<link rel="icon" href="img/brown2.png" />
<link rel="stylesheet" href="styles.css" />
</head>
<body id="home">
<div id="navbar"><span style="font-family: 'DynaPuff', cursive;">yaks.cf</span> | <a href="submit.php">Submit</a></div>
<div id="center">
<h1><span class="txt" style="color:white"><img src="img/white.png" width="50px" height="auto" alt="Kitten not found" /> Submit</span></h1>
<p><span class="txt">Crawler forked from <a href="https://www.youtube.com/watch?v=MRoekZ93bpQ">Yessle</a>'s one. Shoutouts to math cloud 2</span></p>
<form action="get_page.php" method="GET"><input type="url" name="name" placeholder="*your url here*"><input type="submit" value="ADD!"></form>
<div id="logs" style="font-family:monospace;font-size:small;background:#eee;">
<?php if (
  isset($_GET["address"]) &&
  isset($_GET["title"]) &&
  isset($_GET["description"]) &&
  isset($_GET["links"])
) {
  echo htmlentities($_GET["address"]) . "<br />";
  echo htmlentities($_GET["title"]) . "<br /><br />";
  echo htmlentities($_GET["description"]) . "<br /><br />";
  echo htmlentities($_GET["links"]);
} ?>
</div>
<p>
<span class="txt">&copy;2021-2022 Maotei - Hecho en Argentina</span><br>
<span class="txt">Bkg pic by <a href="https://pixabay.com/users/amaralneto2000-9062080/" style="color:white">amaralneto2000</a></span>
</p>
</div>
<script src="script.js"></script>
</body>
</html>