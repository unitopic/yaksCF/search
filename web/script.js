var autosuggest;
var query;
var ftch;
var aClick;
document.getElementById("q").onkeypress = function(event) {
  query = encodeURI(document.getElementById("q").value);
  console.log(query);
  if (query.startsWith("!")) {
    ftch = "/api.php?shortcuts_search=" + query;
  } else {
    //ftch = "/api.php?autosuggest=" + query;
    ftch = "https://repl.db.maotei.ga/autosuggest?query=" + query;
  }
  fetch(ftch)
    .then(response => response.json())
    .then(function(data){
      autosuggest = "";
      first = data[0];
      for (var i = 0; i < 5; i++) {
        if (typeof data[i] !== "undefined") {
          if (query.startsWith("!")) {
            aClick = data[i].split("- ")[0];
          } else {
            aClick = data[i];
          }
          autosuggest += `<li onclick="ac('${aClick}')">${data[i]}</li>`;
        }
      }
      document.getElementById("autosuggest").innerHTML = "<ul>" + autosuggest + "</ul>";
    });
}
function ac(text) {
  document.getElementById("q").value = text;
}
document.getElementById("q").addEventListener("keydown", function(event) {
  if (event.Tab) {
    ac(first);
  }
});
function title_text() {
  fetch("/api.php?srch")
    .then(response => response.text())
    .then(function(data){
      document.getElementById("q").placeholder = data;
    });
}
title_text();
setInterval(title_text, 20000);